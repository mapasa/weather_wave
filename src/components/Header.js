import React, { Component } from 'react';

class Header extends Component {

	render() {

		return(
      <div className="navbar-fixed">
  			<nav>
  				<div className="nav-wrapper blue lighten-1 text-p-l-10">
            <a href="#home" className="brand-logo">WeatherWave</a>
  				</div>
  			</nav>
      </div>
		)

	}
}


export default Header;
