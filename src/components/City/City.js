import React, { Component } from 'react';
import Moment from 'react-moment';
import SeaLevel from './SeaLevel';

// Load the keys file depending upon the environment
const api_config = require('../../config/dev_api');

class City extends Component{

  render(){

    let sunrise_time = new Date( this.props.city.getIn(['weather_info','sunrise']) * 1000);
    let sunset_time  = new Date( this.props.city.getIn(['weather_info','sunset']) * 1000);

  	return (

      <div className="col s12 m12 l6 ">
        {/*
            Number of cards in a row:
            Mobile: 1 city per row
            Tablet: 1 city per row
            Desktop: 2 cities per row
        */}
        <div
          className="card sticky-action hoverable city_card"
          onClick={() => this.props.getSeaLevel(this.props.city.get('city_id'))}
        >

          <div className="card-image waves-effect waves-block waves-light">
            {/*
             Here we are displaying random image from unsplash
             City name is used just so we get different image for each city
            */}
            <img className="activator" src={api_config.unsplashRandomImageURL + "?" + this.props.city.get('name')} alt="{this.props.city.get('name')}" />
          </div>

          <div className="card-content activator">

            <span className="card-title grey-text text-darken-4">{this.props.city.get('name')}
              <i className="material-icons activator right" >more_vert</i>
            </span>

            <div className="divider"></div>

            <div className="section activator">
              <div className="row">

                <div className="col s6 activator">
                    <i className="medium wi wi-sunrise yellow-text text-darken-1"></i>
                </div>

                <div className="col s6 right-align activator">
                    <i className="medium wi wi-sunset"></i>
                </div>

              </div>

              <div className="row">

                <div className="col s6">
                    <Moment format="HH:mm A">{sunrise_time}</Moment>
                </div>

                <div className="col s6 right-align">
                  <Moment format="HH:mm A">{sunset_time}</Moment>
                </div>

              </div>
            </div>
          </div>

          {/*
           The content inside the following div will be displayed when a city is clicked
          */}
          <div className="card-reveal">
            <SeaLevel
              city_id={this.props.city.get('city_id')}
              city_name={this.props.city.get('name')}
            />
          </div>
        </div>
      </div>
  	);
  }

}

export default City;
