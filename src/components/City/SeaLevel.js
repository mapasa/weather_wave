import React, { Component } from 'react';
import { connect } from 'react-redux';
import Moment from 'react-moment';
import {Line} from 'react-chartjs-2';
import moment from 'moment/min/moment.min.js';


class SeaLevel extends Component{

  render(){

    // If the seaLevel data is not retrieved yet, display some default text until it is retrieved
    if(!this.props.seaLevel){
      return(
        <div>
          <span className="card-title grey-text text-darken-4">Retrieving Sea Level...<i className="material-icons right">close</i></span>
        </div>
      )

    }

    // Format the data to be displayed in the chart
    let chart_labels         = [];
    let chart_sea_level_data = [];
    this.props.seaLevel.map(
      (level, index) => {
        // Format the date and add to the graph data
        chart_labels.push(moment(level[0]).format("MMM Do"));

        // Add the sea level
        chart_sea_level_data.push(level[1]);
    });

    // Build the chart data
    let chart_data = {
      labels: chart_labels,
      datasets: [
        {
          label: 'Sea Level',
          fill: true,
          lineTension: 0.1,
          backgroundColor: 'rgba(66, 165, 245, 0.4)',
          borderColor: 'rgba(66, 165, 245,1)',
          borderCapStyle: 'butt',
          borderDash: [],
          borderDashOffset: 0.0,
          borderJoinStyle: 'miter',
          pointBorderColor: 'rgba(191, 227, 255,1)',
          pointBackgroundColor: '#fff',
          pointBorderWidth: 1,
          pointHoverRadius: 5,
          pointHoverBackgroundColor: 'rgba(75,192,192,1)',
          pointHoverBorderColor: 'rgba(220,220,220,1)',
          pointHoverBorderWidth: 2,
          pointRadius: 1,
          pointHitRadius: 10,
          data: chart_sea_level_data
        }
      ]
    };

  	return (
      <div>
        <span className="card-title grey-text text-darken-4">{this.props.city_name}<i className="material-icons right">close</i></span>
        <div className="divider"></div>
        <h6>Sea Level Forecast</h6>
        {/*
          Display the Sea Level chart
        */}
        <Line data={chart_data} />

        {/*
          Display the sea level for the next five days in a table
        */}
        <table className='striped'>
          <thead>
            <tr>
                <th>Date</th>
              <th>Level</th>
            </tr>
          </thead>
          <tbody>
            {/*
              Loop through the sea level data and populate the table
            */}
            {this.props.seaLevel.map(
              (level, index) => {
                return(
                  <tr key={index}>
                    <td>
                      <Moment format="MMMM Do YYYY, h:mm A">{new Date(level[0])}</Moment>
                    </td>
                    <td>{level[1]} mbar</td>
                  </tr>
                )
              }
            )}

          </tbody>
        </table>
      </div>
  	);
  }

}

export default connect(
  (state, props) => ({
    seaLevel: state.getIn(['citySeaLevel',props.city_id])
  }),null
)(SeaLevel);
