import React, { Component } from 'react';
import { connect } from 'react-redux';
import City from './City';
import * as actions from '../../actions';

class CityList extends Component{

  render(){

    // Show loading text until the cities are loaded
    if(!this.props.cities){
      return (
        <div>
          Loading Cities...
        </div>
      )
    }

    // Draw the cities
  	return (
  		<div className="row">
          {
              this.props.cities.map(
                (city) => {
                  return <City key={city.get('city_id')} city={city} getSeaLevel={this.props.getSeaLevel}/>
                }
            )
          }

  		</div>
  	);
  }

}


export default connect(
  (state) => ({
    cities: state.get('cities')
  }),
  actions
)(CityList);
