import React, { Component } from 'react';
import * as actions from '../actions';
import { connect } from 'react-redux';
import CityList from './City/CityList';
import Header from './Header';
import './App.css';

class App extends Component {

  // Let's start with loading the cities on the app is mounted
  componentDidMount(){
		this.props.loadCities();
	}

  render() {
    return (
      <div>
        <Header/>
        <div className="container">
          <CityList/>
        </div>
      </div>
    );
  }
}

export default connect(null, actions)(App);
