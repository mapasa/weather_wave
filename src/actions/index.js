import axios from 'axios';
import {Map, List} from 'immutable';
import { LOAD_CITIES, FETCH_SEA_LEVEL } from './types';

// Load the keys file depending upon the environment
const api_config = require('../config/dev_api');

// We have a list of 100 cities to choose 5 from
const available_cities = [
	"Paris","Rome","London","Berlin","Barcelona","Amsterdam","St. Petersburg","Istanbul","Athens","Copenhagen","Madrid","Brussels","Budapest","Munich","Edinburgh","Prague","Milan","Vienna","Lisbon","Stockholm","Dublin","Red Square","Moscow","Oslo","Florence","Oxford","Cannes","Helsinki","Bruges","Hamburg","Pisa","Dubrovnik","Tallinn","Granada","Salzburg","Bergen","Manchester","Bilbao","Strasbourg","Reykjavik","Naples","Monaco","Bern","Riga","Liverpool","Innsbruck","Luxembourg City","Nice","Cologne","Krakow","Malaga","Warsaw","Genoa","Verona","Thessaloniki","York","Reims","Zurich","Aarhus","Seville","Cordoba","Siena","Belfast","Geneva","Bologna","Zagreb","Bucharest","Vilnius","Rotterdam","Bratislava","Ljubljana","Turin","Assissi","Avignon","Albi","Marseille","Palma De Mallorca","Lubeck","Brighton","Bath"," England","Lyon","Porto","Goteborg","Stavanger","Bayeux","Dusseldorf","Sofia","Split","Valencia","Antalya","Gdansk","Glasgow","Belgrade","Antwerp","Dresden","Mykonos","Palermo","Wroclaw","Zermatt","Lucerne","Inverness"
];


export const loadCities = () => async dispatch => {

	let cities = new List();
	let current_city;

	// Randomly select 5 cities from the list of available cities
	for ( let i = 0; cities.size < 5 && i < available_cities.length; i++ ) {
		current_city = new Map();

		// Select a random city from the list above
		let random_city = available_cities[Math.floor(Math.random() * available_cities.length)];

		// Remove the randomly selected city from the availableCities list to prevent duplicates
		available_cities.splice(available_cities.indexOf(random_city), 1);

		let weather_info;
		try {

			// Get the weather information from Openweathermap
			let openweathermap_weather_url = api_config.openWeatherCurrentDataURL + random_city + "&appid=" + api_config.openWeatherMapAppID;
			weather_info = await axios.get(openweathermap_weather_url);

		} catch( error ){

			// If there was any error retriving the weather info for the current city, continue with a new city from the list
			continue;
		}

		// Check if we have not retrieved the city details from openweathermap
		// If yes, continue with a new city from the list
		if(weather_info !== undefined && weather_info.cod === '404'){
			continue;
		}

		// Build the current city object
		current_city = current_city
			.set('name', random_city)
			.set('weather_info', new Map(weather_info.data.sys))
			.set('city_id', weather_info.data.id);

		// Add the current city tot he cities list
		cities = cities.push(current_city);

		// We dispatch the action immediately so that the cities gets drawn as soon as the data is retrieved
		dispatch({ type: LOAD_CITIES, payload: cities});
	}

};


export const getSeaLevel = ( city_id ) => async (dispatch, getState) => {

	// Check if we have already retrieved the sea level information for the given city_id
	let city_sea_level = getState().get("citySeaLevel");
	if(city_sea_level.get(city_id)){
		return;
	}
	// Get the  weather info for the next 5 days
	let weather_info;

	try {

		// Get the weather information from Openweathermap
		let openweathermap_weather_url = api_config.openWeather5DayForecastURL + city_id + "&appid=" + api_config.openWeatherMapAppID;
		weather_info = await axios.get(openweathermap_weather_url);

	} catch( error ){

		// If there was any error retriving the weather info for the current city, continue with a new city from the list
		alert("Error retrieving weather forcast. Please try again later.");
	}

	let sea_level    = [];
	let current_date = new Date();

	/*
		Loop through the data retrievef from openweathermap,
		filter the sea level information at 9:00 am each day
		and build the sea level object
	*/
	weather_info.data.list.forEach( (weather) => {
		let weather_date_time = new Date(weather.dt_txt);

		if(weather_date_time.getHours() === 9 && weather_date_time.getDate() !== current_date.getDate() ){
			sea_level.push([weather.dt_txt, weather.main.sea_level]);

		}

	});

	dispatch({ type: FETCH_SEA_LEVEL, payload: {city_id, sea_level} });


}
