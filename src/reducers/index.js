import { LOAD_CITIES, FETCH_SEA_LEVEL } from '../actions/types';
import {Map, List} from 'immutable';

// Define the initial state of the app
const initialState = new Map({
  isCitiesLoaded: false,
  cities: new List(),
  citySeaLevel: new Map()
});

// slice state and delegate to each reducer.
export default function(state = initialState, action) {

  switch(action.type){

		case LOAD_CITIES:
      return state.set('cities', action.payload);

    case FETCH_SEA_LEVEL:
			return state.setIn(['citySeaLevel', action.payload.city_id], action.payload.sea_level);

		default:
			return state;
	}

}
