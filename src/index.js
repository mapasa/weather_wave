import 'materialize-css/dist/css/materialize.min.css';
import 'materialize-css/dist/js/materialize.min.js';
import React from 'react';
import ReactDOM from 'react-dom';
import reduxThunk from 'redux-thunk';
import { Provider } from 'react-redux';
import {createStore, applyMiddleware} from 'redux';
import App from './components/App';
import reducers from './reducers';
import registerServiceWorker from './registerServiceWorker';

// Create the store with the redux-thunk middleware
const Store = createStore(
	reducers,
	undefined,
	applyMiddleware(reduxThunk)
);

ReactDOM.render(
	<Provider store={Store}><App /></Provider>,
	document.querySelector('#root')
);

registerServiceWorker();
