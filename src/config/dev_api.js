// Define all the keys & external links used in the development environment
module.exports = {
	openWeatherMapAppID : "3d8b309701a13f65b660fa2c64cdc517",
	openWeatherCurrentDataURL: "http://api.openweathermap.org/data/2.5/weather?q=",
	openWeather5DayForecastURL: "http://api.openweathermap.org/data/2.5/forecast?id=",
	unsplashRandomImageURL: "https://source.unsplash.com/random/300x300"

}
